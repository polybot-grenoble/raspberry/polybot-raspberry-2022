import socket
import threading
import queue
import time

QUEUE_SIZE = 100
READ_BUFFER_SIZE = 256





class ClientComPoint:
    """Class that handles communication with the server"""
    
    def __init__(self, ip, port, name):
        """Initializes a client which will connect to the given ip and port"""
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.name = name
        self.ip = ip
        self.port = port
        self.readingThread = ReadingThread(self,self.s)
        self.q = queue.Queue(QUEUE_SIZE)

        print('Client initialized')

    def connect(self):
        """Connects to the server"""
        self.s.connect((self.ip, self.port))
        self.readingThread.start()
        print(f'Connected to {self.ip}:{self.port}')

    def write(self, data):
        """Sends data to the server"""
        
        n = self.s.send(data)
        if n < len(data):
            print(f'sent {n} bytes instead of {len(data)}')
            return 0
        print(f'sent {len(data)} bytes')

    def read(self):
        """Checks if data was received and returns it. Returns [] otherwise."""

        try:
            data = self.q.get(timeout=1)
        except queue.Empty:
            return []
        return data
        
        
class Client:
    """Client that connects to the server"""

    def __init__(self,name):
        self.comPoint = ClientComPoint("127.0.0.1",57575,name)
        




server = Server("Access Point")
client = Client("Robot 1")

server.comPoint.create()

time.sleep(0.1)
client.comPoint.connect()
time.sleep(0.1)

clientAddress = list(server.comPoint.clientList.keys())[0]

client.comPoint.write(bytes([0x13, 0x00, 0x00, 0x00, 0x08, 0x00]))
server.comPoint.write(clientAddress,bytes([0x05, 0x05, 0x05, 0x05, 0x05, 0x05]))

while True:
    a = server.comPoint.read(clientAddress)
    if a:
        print(a)
    b = client.comPoint.read()
    if b:
        print(b)
