import socket
import time
import math
import numpy as np
import json
import cv2

IMAGE_W = 500
IMAGE_H = 500
ROBOT_W = 213
ROBOT_L = 216
SCALE = 0.5

print(socket.gethostname())
print(socket.gethostbyname(socket.gethostname()))

class Client:
    """Client"""

    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def write(self, data):
        """send data to server"""
        n = self.s.send(data)
        if n < len(data):
            print(f"sent {n} bytes instead of {len(data)}")

    def read(self, n):
        """read `n` bytes from server"""
        if self.s is None:
            return None
        return self.s.recv(n)

    def connect(self):
        r = -1
        while r != 0:
            # return 0 when connection succeed
            r = self.s.connect_ex(("cuisinier", 57575))
        print("Connected to server")

def generateTarget(img):
    
    for i in range(1,6):
        for j in range(0,360):
            x = int(math.cos(math.radians(j))*SCALE*(i-1)*100 + IMAGE_W/2)
            y = int(math.sin(math.radians(j))*SCALE*(i-1)*100 + IMAGE_H/2)
            if i >= 1 and i <= 4: # Stop bounds [10,40]
                img[x,y,:] = 125
                img[x,y,2] = 175
            else:
                img[x,y,:] = 125
    for i in range(0,250):
        img[int(IMAGE_W/2),i,:] = 125
        img[i,int(IMAGE_W/2),:] = 125
        img[i,i,:] = 125
        img[i,IMAGE_W-1-i,:] = 125

    for i in range(int(ROBOT_W*SCALE)):
        x = int(IMAGE_W/2 - ROBOT_W * SCALE /2)
        y1 = int(IMAGE_W/2 - ROBOT_L * SCALE /2)
        y2 = int(IMAGE_W/2 + ROBOT_L * SCALE /2)
        img[y1,x+i,:] = 255
        img[y2,x+i,:] = 255

    for i in range(int(ROBOT_L*SCALE)):
        y = int(IMAGE_W/2 - ROBOT_L * SCALE /2)
        x1 = int(IMAGE_W/2 - ROBOT_W * SCALE /2)
        x2 = int(IMAGE_W/2 + ROBOT_W * SCALE /2)
        img[y+i,x1,:] = 255
        img[y+i,x2,:] = 255

c = Client()
c.connect()

targetImg = np.zeros((IMAGE_W,IMAGE_H,3),np.uint8)
generateTarget(targetImg)
while True:
    data = c.read(10000).decode("utf-8")
    if len(data) == 0:
        time.sleep(0.1)
        continue
    print(f"Received {len(data)} bytes")
    p_data = {}
    for couple in data.split('/'):
        if couple == '':
            continue
        vals = couple.split(':')
        if len(vals) != 2 or vals[0] == '' or vals[1] == '':
            continue
        p_data[float(vals[0])] = float(vals[1])

    lidarImg = np.copy(targetImg)                                                             
                            
    for angle,dist in p_data.items():
        if dist*SCALE >= IMAGE_W/2:
            continue
        x = int(math.cos(math.radians(angle)) * dist * SCALE+IMAGE_W/2)
        y = int(math.sin(math.radians(angle)) * dist * SCALE+IMAGE_W/2)
        #if dist >= 150 and dist <= 450 and angle >= 0 and angle <= 90:
        #    cv2.circle(lidarImg,(x,y),4,(0,0,255),4)
        #else:
        cv2.circle(lidarImg,(x,y),4,(0,255,0),4)

    cv2.imshow("Lidar", lidarImg)
    cv2.waitKey(100)
    
