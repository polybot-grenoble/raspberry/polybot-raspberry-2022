import serial
import time
import numpy as np
import math
import cv2
import matplotlib.pyplot as plt
import pylab

IMAGE_W = 500
IMAGE_H = 500
ROBOT_W = 213
ROBOT_L = 216
SCALE = 0.5


    

def generateTarget(img):     
    
    for i in range(1,6):
        for j in range(0,360):
            x = int(math.cos(math.radians(j))*SCALE*(i-1)*100 + IMAGE_W/2)
            y = int(math.sin(math.radians(j))*SCALE*(i-1)*100 + IMAGE_H/2)
            if i >= 1 and i <= 4: # Stop bounds [10,40]
                img[x,y,:] = 125
                img[x,y,2] = 175
            else:
                img[x,y,:] = 125
    for i in range(0,250):
        img[int(IMAGE_W/2),i,:] = 125
        img[i,int(IMAGE_W/2),:] = 125
        img[i,i,:] = 125
        img[i,IMAGE_W-1-i,:] = 125

    for i in range(int(ROBOT_W*SCALE)):
        x = int(IMAGE_W/2 - ROBOT_W * SCALE /2)
        y1 = int(IMAGE_W/2 - ROBOT_L * SCALE /2)
        y2 = int(IMAGE_W/2 + ROBOT_L * SCALE /2)
        img[y1,x+i,:] = 255
        img[y2,x+i,:] = 255

    for i in range(int(ROBOT_L*SCALE)):
        y = int(IMAGE_W/2 - ROBOT_L * SCALE /2)
        x1 = int(IMAGE_W/2 - ROBOT_W * SCALE /2)
        x2 = int(IMAGE_W/2 + ROBOT_W * SCALE /2)
        img[y+i,x1,:] = 255
        img[y+i,x2,:] = 255

def drawPoint(L,cible):
    for i in range(0,len(L)):
        for k in range(0,len(L[0])):
            angle=L[i][k]['angle']
            distance=L[i][k]['distance']
            x = int(math.cos(math.radians(angle)) * distance * SCALE+IMAGE_W/2)
            y = int(math.sin(math.radians(angle)) * distance * SCALE+IMAGE_W/2)
            #cv2.circle(cible,(x,y),4,(0,255,0),4)
            if (x!=0 and x<1000 and x>-1000) and (y!=0 and y<1000 and y>-1000):
                plt.plot([x],[y],'ro')
    print("%AFFICHAGE%")
    
    plt.show()
    #cv2.imshow("Lidar", cible)
    return 0


targetImg = np.zeros((IMAGE_W,IMAGE_H,3),np.uint8)
generateTarget(targetImg)
lidarImg = np.copy(targetImg) 


def decryptdata(data):

    # If header is 0x6A then it's the first data (1 °C) otherwise its 0x5A
    if data[0] != '10100101' and (data[1] != '01101010' or data[1] != '01011010'):
        print("Error wrong header format: " + str(data[0]) + " " + str(data[1]))
        return

    binflag=data[2][0]
    if binflag == '0':
        print("_____________________")
        print(" ")
        print("Output Angle")
    else:
        print("Output Intensity")

    binspeed=data[2][1::]+data[3]
    speed=int(binspeed,2)
    print("Speed : ",speed)
    
    binangular_res=data[4][0:7]
    angular_res=int(binangular_res,2)
    print("Angular Resolution : ", angular_res)

    binstart_angle=data[4][7]+data[5]
    start_angle=int(binstart_angle,2)
    print("Start Angle : ",start_angle)
    print("_____________________")
    print("")

    data_array = [0]*15
    angle=start_angle
    for i in range(0,15):
        if i!=0:
            angle += angular_res/100
        #float(int(data[6+3*i],2))/4
        else:
            angle=angle

        distance = float(int((data[7+3*i]+data[8+3*i]),2))
        data_array[i]={"angle":angle,"distance":distance}
    
        
        
    
    cv2.waitKey(100)
    
    

    return data_array   

#######################################################################

ser = serial.Serial('COM9', 460800)
print(ser.name)

startcommand = bytearray([0xA5, 0x2C])
switchcommand_angle= bytearray([0xA5, 0x5C])
stopcommand = bytearray([0xA5, 0x25])
measurelapcommand = bytearray([0xA5,0x22])
setangularres=bytearray([0xA5,0x30,0x00,0x64])
measurecommand=bytearray([0xA5,0x20])

ser.write(stopcommand)
cv2.waitKey(2000)
ser.write(startcommand)
cv2.waitKey(1000)

ser.write(setangularres)

t = time.time()
mesure=0
L=[None]*24
z=True
while z==True:

    p=1
    #int(input("1 pour singlelap / 0 pour exit"))
    if p==0:
        break
    elif p==1:
        ser.write(measurelapcommand)
    else:
        ser.write(measurecommand)
    time.sleep(1)
    while z==True:
        r= ser.read(51)
        #print(r)
        if type(r)!= type(bytes(1)):
            break
    
        liste=list(r)
        r=[bin(x) for x in liste]
        for i in range(0,51):
            r[i]=r[i][2::].rjust(8,'0')
    
        
        L[mesure]=decryptdata(r)
        mesure+=1
        if mesure==24:
            z=False
            print(L)
        cv2.waitKey(1)
    

print("Fin Mesure")
ser.write(stopcommand)
ser.close()

drawPoint(L,lidarImg)
