import serial
import time



ser = serial.Serial('COM9', 460800)
print(ser.name)

startcommand = bytearray([0xA5, 0x2C])
stopcommand = bytearray([0xA5, 0x25])
measurecommand = bytearray([0xA5,0x22])
ser.write(startcommand)


a=True
while a==True:
    rpm1=int(input("Valeur rpm: "))
    rpm='0x'+hex(rpm1)[2::].rjust(2,'0') #concerti rpm1 en forma hex (normalement)
    print(rpm)
    if rpm1!=0:
        ser.write(measurecommand)
        time.sleep(1)
        print(rpm1)
        ser.write(bytearray([0xA5,0x20,int(rpm1)])) #j'essaye d'envoyer la valeur au lydar
        r= ser.read(186) #La suite je recup la valeur de speed a la facon du LS01B.py

        liste=list(r)
        r=[bin(x) for x in liste]
        for i in range(0,len(r)):
            r[i]=r[i][2::].rjust(8,'0')
        binspeed=r[2][1::]+r[3]
        speed=int(binspeed,2)
        print("Speed : ",speed)
    else:
        a=False
ser.write(stopcommand)